from django.db import models
from reusable_models import get_model_from_string

StaffDesignation = get_model_from_string("STAFF_DESIGNATION")
Designation = get_model_from_string("DESIGNATION")


class AccountantManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(designation__name="Accountant")


class Accountant(StaffDesignation):
    objects = AccountantManager()

    class Meta:
        proxy = True
